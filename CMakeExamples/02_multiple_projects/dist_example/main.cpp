#include <iostream>
#include <FirstClass.h>

int main() {
    FirstClass cls;
    cls.Set("OK");
    std::cout << cls.Get() << std::endl;
    return 0;
}
