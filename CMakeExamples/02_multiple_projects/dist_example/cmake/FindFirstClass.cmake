message(info $ENV{FirstClass_ROOT})

cmake_policy(SET CMP0074 NEW)
find_library(
    FirstClass_LIBRARIES
    FirstClass
    HINTS
        $ENV{FirstClass_ROOT}/lib
        ${FirstClass_ROOT}/lib
)                                            
                                          
find_path(                            
    FirstClass_INCLUDE_DIRS           
    FirstClass.h
    HINTS
        $ENV{FirstClass_ROOT}/include
        ${FirstClass_ROOT}/include
)

set(FirstClass_VERSION 0.1)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FirstClass
    FOUND_VAR FirstClass_FOUND
    REQUIRED_VARS
        FirstClass_LIBRARIES
        FirstClass_INCLUDE_DIRS
    VERSION_VAR FirstClass_VERSION
)
